var my_array = require('./my_array');
var patterns = require('./patterns');
var filename = process.argv[2];
if(typeof(filename) == 'undefined') {
  filename = 'sample.log';
}

var fs = require('fs');
var patterns_config = patterns.all;
var results = {};

//Initialize results
var keys = Object.keys(patterns_config);
for(i = 0; i < keys.length; i++) {
  var key = keys[i];
  results[patterns_config[key].key] = {
    url            : patterns_config[key].url,
    count          : 0,
    response_times : [],
    dynos_count    : {}
  }
}
//=====================================

function readLines(input) {
  var remaining = '';

  input.on('data', function(data) {
    remaining += data;
    var index = remaining.indexOf('\n');
    while (index > -1) {
      var line = remaining.substring(0, index);
      remaining = remaining.substring(index + 1);
      analyze(line);
      index = remaining.indexOf('\n');
    }
  });

  input.on('end', function() {
    print_results();
  });
}

function print_results() {
  console.log("*******************************************************************");
  console.log("******************************URL REPORT***************************");
  console.log("*******************************************************************");
  Object.keys(results).forEach(function(key) {
    var val = results[key];
    console.log(val.url);
    console.log("\tThe number of times the URL was called => " + val.response_times.length);
    console.log("\tAverage Response Time => " + my_array.mean(val.response_times));
    console.log("\tResponse Time Median => "  + my_array.median(val.response_times));
    console.log("\tResponse Time Mode => "    + my_array.mode(val.response_times));
    console.log("\tMost Active Dyno => "      + my_array.most_active_dyno(val.dynos_count));
  });
}

function analyze(line) {
  var line_data = {};
  line.split(" ").forEach(function(val, index, array) {
    var splits = val.split("=");
    if(splits.length == 2) {
      line_data[splits[0]] = splits[1];
    }
  });

  var target;
  Object.keys(patterns_config).forEach(function(key) {
    val = patterns_config[key];
    if(line_data["method"] == val.method && line_data["path"].search(val.path) != -1) {
      target = val;
      return false;
    }
  });

  if(target) {
    key = target.key;
    if(key) {
      results[key].count += 1;
      results[key].response_times.push(parseInt(line_data["connect"]) + parseInt(line_data["service"]));
      if(results[key].dynos_count[line_data["dyno"]]) {
        results[key].dynos_count[line_data["dyno"]] += 1
      } else {
        results[key].dynos_count[line_data["dyno"]]  = 1
      }
    }
  }
}

if(fs.existsSync(filename)) {
  var input = fs.createReadStream(filename);
  readLines(input);
} else {
  console.log("The file you specified does not exist");
}
