exports.all = {
  pending_messages : {
                       method : 'GET',
                       path   : /\/api\/users\/\d+\/count_pending_messages$/,
                       key    : 'get_count_pending_messages',
                       url    : '/api/users/{user_id}/count_pending_messages'
                     },
  messages : {
               method : 'GET',
               path   : /\/api\/users\/\d+\/get_messages$/,
               key    : 'get_get_messages',
               url    : '/api/users/{user_id}/get_messages'
             },
  friends_progress : {
                       method : 'GET',
                       path   : /\/api\/users\/\d+\/get_friends_progress$/,
                       key    : 'get_get_friends_progress',
                       url    : '/api/users/{user_id}/get_friends_progress'
                     },
  friends_score : {
                    method : 'GET',
                    path   : /\/api\/users\/\d+\/get_friends_score$/,
                    key    : 'get_get_friends_score',
                    url    : '/api/users/{user_id}/get_friends_score'
                  },
  post_users : {
                 method : 'POST',
                 path   : /\/api\/users\/\d+$/,
                 key    : 'post_users',
                 url    : '/api/users/{user_id}'
               },
  get_users : {
                method : 'GET',
                path   : /\/api\/users\/\d+$/,
                key    : 'get_users',
                url    : '/api/users/{user_id}'
              }
}
