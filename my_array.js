exports.mean = function(arr) {
  if(!arr.length) {
    return 0;
  }
  var sum = arr.reduce(function(p, c, index, array) {
    return p + c;
  });
  var total = arr.length;
  return (sum / total).toFixed(2);
};

exports.median = function(arr) {
  if(!arr.length) {
    return null;
  }
  arr = arr.sort();
  var total = arr.length; var mid = parseInt(total / 2);
  if(total % 2 == 0) {
    return ((arr[mid-1] + arr[mid]) / 2).toFixed(2);
  } else {
    return arr[mid].toFixed(2);
  }
};

exports.mode = function(arr) {
  if(arr.length == 0)
    return null;
  var mode_map = {};
  var max_element = arr[0], max_count = 1;
  for(var i = 0; i < arr.length; i++) {
    var el = arr[i];
    if(mode_map[el] == null)
      mode_map[el] = 1;
    else
      mode_map[el]++;
    if(mode_map[el] > max_count) {
      max_element = el;
      max_count = mode_map[el];
    }
  }
  return max_element;
};

exports.most_active_dyno = function(dynos_count) {
  var winner_dyno;
  var responses = 0;

  Object.keys(dynos_count).forEach(function(index, val, array) {
    var dyno = index;
    var count = dynos_count[index];
    if(count > responses) {
      winner_dyno = dyno;
      responses = count;
    }
  });

  if(typeof(winner_dyno) == 'undefined') {
    return "N/A";
  } else {
    return winner_dyno + " wins with " + responses + " responses";
  }
};
